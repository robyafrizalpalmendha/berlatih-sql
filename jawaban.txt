Soal 1 Membuat Database
    create database myshop;

Soal 2 Membuat Table di Dalam Database
    Categories
        create table categories(id int(8) auto_increment, name varchar(255), primary key(id));
    Items
        create table items(id int(8) auto_increment, name varchar(255), description varchar(255), price int(10), stock int(5), category_id int(8), primary key(id), foreign key(category_id) references categories(id));
    Users
        create table users(id int(8) auto_increment, name varchar(255), email varchar(255), password varchar(255), primary key(id));

Soal 3 Memasukkan Data pada Table
    Users
        insert into users(name, email, password) values("John Doe", "john@doe.com", "john123"),("Jane Doe", "jane@doe.com", "jenita123");
    Categories
        insert into categories(name) values("gadget"),("cloth"),("men"),("women"),("branded");
    Items
        insert into items(name, description, price, stock, category_id) values("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1),("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2),("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);

Soal 4 Mengambil Data dari Database
a. Mengambil data users (Kecuali Password)
    select name, email from users;
b. Mengambil data items
    Buatlah sebuah query untuk mendapatkan data item pada table items yang memiliki harga di atas 1000000 (satu juta).
        select * from items where price > 1000000;

    Buat sebuah query untuk mengambil data item pada table items yang memiliki name serupa atau mirip (like) dengan kata kunci “uniklo”, “watch”, atau “sang”.
        select * from items where name like ("%uniklo%");
        select * from items where name like ("%watch%");
        select * from items where name like ("%sang%");

c. Menampilkan data items join dengan kategori
    select items.name, items.description, items.price, items.stock, items.category_id, categories.name as kategori from items inner join categories on items.category_id = categories.id;

Soal 5 Mengubah Data dari Database
    update items set price = 2500000 where id = 1;


Soal QUIZ - sintaks SQL untuk mendapatkan data yang menghitung total belanja dari setiap customer
    select customers.name as customers_name, sum(orders.amount) as total_amount from customers inner join orders on customer_id = orders.customers_id group by customers.id
    ATAU
    select customers.name as customers_name, sum(orders.amount) as total_amount from orders inner join customers on orders.customer_id = customers.id group by customers.name order by customers.name desc;



nb:
//Menjalankan MAMP di terminal : mysql -u root -p (password: root)
//Membuat database : create database myshop;
//Memilih Database : use myshop;
//Menampilkan database : show databases;
//Menampilkan table : show tables;
//Status Database : status
//Melihat deskripsi tabel : describe users;
//Melihat Isi  Table : select * from users;
//Menghapus Table : delete from users where id = 2;
//Menghapus database : drop database myshop;
//Keluar mysql : exit